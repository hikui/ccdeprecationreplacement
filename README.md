# CCDeprecationReplacement

[![CI Status](http://img.shields.io/travis/Henry Heguang Miao/CCDeprecationReplacement.svg?style=flat)](https://travis-ci.org/Henry Heguang Miao/CCDeprecationReplacement)
[![Version](https://img.shields.io/cocoapods/v/CCDeprecationReplacement.svg?style=flat)](http://cocoapods.org/pods/CCDeprecationReplacement)
[![License](https://img.shields.io/cocoapods/l/CCDeprecationReplacement.svg?style=flat)](http://cocoapods.org/pods/CCDeprecationReplacement)
[![Platform](https://img.shields.io/cocoapods/p/CCDeprecationReplacement.svg?style=flat)](http://cocoapods.org/pods/CCDeprecationReplacement)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CCDeprecationReplacement is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CCDeprecationReplacement'
```

## Author

Henry Heguang Miao, heguang.miao@coroma.com.au

## License

CCDeprecationReplacement is available under the MIT license. See the LICENSE file for more info.
