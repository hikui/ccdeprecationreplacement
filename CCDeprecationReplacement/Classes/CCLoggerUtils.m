//
//  CCLoggerUtils.m
//  CCDeprecationReplacement
//
//  Created by Henry Miao on 20/12/17.
//

#import "CCLoggerUtils.h"

@implementation CCLoggerUtils

+ (DDLogFlag)ddLogFlagForLogLevel:(DDLogLevel)level {
    switch (level) {
        case DDLogLevelAll:
            return DDLogFlagVerbose;
        case DDLogLevelVerbose:
            return DDLogFlagVerbose;
        case DDLogLevelInfo:
            return DDLogFlagInfo;
        case DDLogLevelDebug:
            return DDLogFlagDebug;
        case DDLogLevelWarning:
            return DDLogFlagWarning;
        case DDLogLevelError:
            return DDLogFlagError;
        default:
            return DDLogFlagError;
    }
}

@end
