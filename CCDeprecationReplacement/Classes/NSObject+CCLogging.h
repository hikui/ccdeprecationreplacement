//
//  NSObject+CCLogging.h
//  CCDeprecationReplacement
//
//  Created by Henry Miao on 21/11/17.
//

#import <Foundation/Foundation.h>
#import <CocoaLumberjack/CocoaLumberjack.h>

@interface NSObject (CCLogging)

// Replaceme
- (void)cc_log:(DDLogLevel)level msg:(NSString *)message;
- (void)cc_log:(DDLogLevel)level format:(NSString *)format, ...;

@end
