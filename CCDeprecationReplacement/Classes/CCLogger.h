//
//  CCLogger.h
//  CCForce
//
//  Created by Henry Heguang Miao on 16/10/17.
//  Copyright © 2017 Coroma Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CocoaLumberjack/CocoaLumberjack.h>
@interface CCLogger : NSObject

@property (nonatomic) DDLogLevel logLevel;

+ (CCLogger *)sharedInstance;

+ (DDLogLevel)logLevel;

+ (void)setLogLevel:(DDLogLevel)logLevel;

+ (void)cc_log:(DDLogLevel)level msg:(NSString *)message;
+ (void)cc_log:(DDLogLevel)level format:(NSString *)format, ...;

- (void)cc_log:(BOOL)asynchronous
         level:(DDLogLevel)level
          flag:(DDLogFlag)flag
       context:(NSInteger)context
          file:(const char *)file
      function:(const char *)function
          line:(NSUInteger)line
           tag:(id)tag
        format:(NSString *)format
          args:(va_list)args;

@end
