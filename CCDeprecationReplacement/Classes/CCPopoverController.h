//
//  CCPopoverController.h
//  TotalFace
//
//  Created by Henry Heguang Miao on 1/9/17.
//  Copyright © 2017 Coroma Consulting All rights reserved.
//

#import <UIKit/UIKit.h>

@class CCPopoverController;
@protocol CCPopoverControllerDelegate <NSObject>

@optional
- (void)popoverControllerDidDismissPopover:(nonnull CCPopoverController *)popoverController;

@end

/**
 Drop-in substitution of CCPopoverController with minimal available methods.
 Used in adapting new popover methods without changing existing code too much.
 */
@interface CCPopoverController : NSObject

@property (nullable, nonatomic, weak) id<CCPopoverControllerDelegate> delegate;

@property(nonatomic) CGSize popoverContentSize;

- (nonnull instancetype)initWithContentViewController:(nonnull UIViewController *)controller;

- (void)presentPopoverFromRect:(CGRect)rect
                        inView:(nonnull UIView *)view
      permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections
                      animated:(BOOL)animated;


- (void)dismissPopoverAnimated:(BOOL)animated;

@end
