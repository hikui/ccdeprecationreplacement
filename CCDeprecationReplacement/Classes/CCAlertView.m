//
//  TFAlertView.m
//  TotalFace
//
//  Created by Henry Heguang Miao on 30/8/17.
//  Copyright © 2017 Coroma Consulting All rights reserved.
//

#import "CCAlertView.h"
#import "NSObject+CCLogging.h"

@interface CCAlertView ()

@property (nullable, nonatomic, weak) id<CCAlertViewDelegate> delegate;
@property (nonnull, nonatomic, strong) NSArray<NSString *> *buttonTitles;

@property (nonatomic) NSInteger cancelButtonIndex;

@property (nonatomic, strong) UIWindow *alertWindow;

- (void)internalShow;

@end

#pragma mark -

@interface CCAlertViewPresentingQueue: NSObject

@property (nonnull, nonatomic, strong) NSMutableArray<CCAlertView *> *alertViewQueue;
@property (nonatomic) BOOL isPresenting;

+ (instancetype)sharedInstance;

- (void)addAlertViewAndShow:(CCAlertView *)alertView;
- (void)notifyDissmissal:(CCAlertView *)alertView;

@end

@implementation CCAlertViewPresentingQueue

+ (instancetype)sharedInstance {
    static CCAlertViewPresentingQueue *shared;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[CCAlertViewPresentingQueue alloc]init];
    });
    return shared;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _alertViewQueue = [NSMutableArray new];
    }
    return self;
}

- (void)addAlertViewAndShow:(CCAlertView *)alertView {
    [self.alertViewQueue addObject:alertView];
    [self checkQueueAndShow];
}

- (void)notifyDissmissal:(CCAlertView *)alertView {
    [self.alertViewQueue removeObject:alertView];
    self.isPresenting = NO;
    [self checkQueueAndShow];
}

- (void)checkQueueAndShow {
    if (self.isPresenting) {
        return;
    }

    CCAlertView *head = self.alertViewQueue.firstObject;
    if (!head) {
        return;
    }

    [head internalShow];
    self.isPresenting = YES;
}

@end

#pragma mark -

@implementation CCAlertView

+ (nonnull instancetype)ccAlertViewWithTitle:(nullable NSString *)title message:(nullable NSString *)message delegate:(nullable id<CCAlertViewDelegate>)delegate cancelButtonTitle:(nullable NSString *)cancelButtonTitle otherButtonTitles:(nullable NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION {
    CCAlertView *alertView = [self alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    alertView.delegate = delegate;

    NSMutableArray *buttonTitles = [NSMutableArray new];
    va_list argumentList;
    id eachObject;
    if (otherButtonTitles) {
        [buttonTitles addObject:otherButtonTitles];
        va_start(argumentList, otherButtonTitles);
        while ((eachObject = va_arg(argumentList, id))) {
            [buttonTitles addObject:eachObject];
        }
        va_end(argumentList);
    }

    [alertView internalInitWithCancelButtonTitle:cancelButtonTitle otherButtonTitles:[buttonTitles copy]];

    return alertView;
}

- (void)internalInitWithCancelButtonTitle:(nullable NSString *)cancelButtonTitle
                        otherButtonTitles:(nullable NSArray<NSString *> *)otherButtonTitles {

    NSMutableArray<NSString *> *buttonTitles = [NSMutableArray new];

    __weak typeof(self) weakSelf = self;

    if (cancelButtonTitle) {
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [weakSelf buttonOnTap:action.title];
        }];
        [self addAction:cancelAction];
        [buttonTitles addObject:cancelButtonTitle];

        self.cancelButtonIndex = 0;
    } else {
        self.cancelButtonIndex = -1;
    }

    if (otherButtonTitles) {
        for (NSString *buttonTitle in otherButtonTitles) {
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:buttonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [weakSelf buttonOnTap:action.title];
            }];
            [self addAction:cancelAction];
        }

        [buttonTitles addObjectsFromArray:otherButtonTitles];
    }

    self.buttonTitles = [buttonTitles copy];
}

- (void)buttonOnTap:(NSString *)buttonTitle {
    NSInteger buttonIndex = [self.buttonTitles indexOfObject:buttonTitle];
    if (buttonIndex != NSNotFound) {
        if ([self.delegate respondsToSelector:@selector(alertView:clickedButtonAtIndex:)]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate alertView:self clickedButtonAtIndex:buttonIndex];
            });
        }
    }
}

- (void)presentedByViewController:(nonnull UIViewController *)viewController {
    [viewController presentViewController:self animated:YES completion:nil];
}

- (nullable NSString *)buttonTitleAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex >= self.buttonTitles.count) {
        return nil;
    }

    return self.buttonTitles[buttonIndex];
}

// A hacky way to simulate UIAlertView behaviour
// https://stackoverflow.com/a/30941356/816799

- (void)show {
    [[CCAlertViewPresentingQueue sharedInstance]addAlertViewAndShow:self];
}

- (void)internalShow {
    self.alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.alertWindow.rootViewController = [[UIViewController alloc] init];

    id<UIApplicationDelegate> delegate = [UIApplication sharedApplication].delegate;
    // Applications that does not load with UIMainStoryboardFile might not have a window property:
    if ([delegate respondsToSelector:@selector(window)]) {
        // we inherit the main window's tintColor
        self.alertWindow.tintColor = delegate.window.tintColor;
    }

    // window level is above the top window (this makes the alert, if it's a sheet, show over the keyboard)
    UIWindow *topWindow = [UIApplication sharedApplication].windows.lastObject;
    self.alertWindow.windowLevel = topWindow.windowLevel + 1;

    [self.alertWindow makeKeyAndVisible];
    [self.alertWindow.rootViewController presentViewController:self animated:YES completion:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.alertWindow.hidden = YES;
    self.alertWindow = nil;
    [[CCAlertViewPresentingQueue sharedInstance]notifyDissmissal:self];
}

- (void)dealloc {
    [self cc_log:DDLogLevelDebug msg:@"Dealloc'ing"];
}

@end
