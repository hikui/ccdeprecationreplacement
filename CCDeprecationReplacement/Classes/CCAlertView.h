//
//  TFAlertView.h
//  TotalFace
//
//  Created by Henry Heguang Miao on 30/8/17.
//  Copyright © 2017 Coroma Consulting All rights reserved.
//

#import <UIKit/UIKit.h>

@class CCAlertView;
@protocol CCAlertViewDelegate <NSObject>

@optional
- (void)alertView:(nonnull CCAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

@end

/**
 Drop-in substitution of UIAlertView with minimal available methods.
 Used in adapting UIAlertController without changing existing code too much.
 */
@interface CCAlertView : UIAlertController

@property (nonatomic) NSInteger tag;

+ (nonnull instancetype)ccAlertViewWithTitle:(nullable NSString *)title message:(nullable NSString *)message delegate:(nullable id<CCAlertViewDelegate>)delegate cancelButtonTitle:(nullable NSString *)cancelButtonTitle otherButtonTitles:(nullable NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;

/**
 Specify a view controller to present the alert view.
 Prefer this method over `show`.

 @param viewController UIViewController.
 */
- (void)presentedByViewController:(nonnull UIViewController *)viewController;

- (nullable NSString *)buttonTitleAtIndex:(NSInteger)buttonIndex;

- (NSInteger)cancelButtonIndex;

- (void)show;

@end
