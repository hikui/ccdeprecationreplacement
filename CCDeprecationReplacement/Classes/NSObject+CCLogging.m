//
//  NSObject+CCLogging.m
//  CCDeprecationReplacement
//
//  Created by Henry Miao on 21/11/17.
//

#import "NSObject+CCLogging.h"
#import "CCLogger.h"
#import "CCLoggerUtils.h"

@implementation NSObject (CCLogging)

- (void)cc_log:(DDLogLevel)level msg:(NSString *)message {
    [self cc_log:level format:@"%@", message];
}

- (void)cc_log:(DDLogLevel)level format:(NSString *)format, ... {
    CCLogger *logger = [CCLogger sharedInstance];
    va_list args;
    va_start(args, format);
    
    [logger cc_log:level != DDLogLevelError
          level:level
           flag:[CCLoggerUtils ddLogFlagForLogLevel:level]
        context:0
           file:nil
       function:nil
           line:0
            tag:[self class]
         format:format
           args:args];
    
    va_end(args);
}

@end
