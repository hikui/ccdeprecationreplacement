//
//  CCPopoverController.m
//  TotalFace
//
//  Created by Henry Heguang Miao on 1/9/17.
//  Copyright © 2017 Coroma Consulting All rights reserved.
//

#import "CCPopoverController.h"
#import "NSObject+CCLogging.h"

@interface CCPopoverController () <UIPopoverPresentationControllerDelegate>

@property (nonatomic, strong) UIViewController *contentController;

@end

@implementation CCPopoverController

- (instancetype)initWithContentViewController:(nonnull UIViewController *)controller {
    self = [super init];
    if (self) {
        _contentController = controller;
    }
    return self;
}

- (void)dealloc {
    [self cc_log:DDLogLevelDebug msg:@"CCPopoverController deallocated"];
}

- (void)presentPopoverFromRect:(CGRect)rect
                        inView:(nonnull UIView *)view
      permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections
                      animated:(BOOL)animated {

    self.contentController.modalPresentationStyle = UIModalPresentationPopover;
    self.contentController.popoverPresentationController.sourceRect = rect;
    self.contentController.popoverPresentationController.sourceView = view;
    self.contentController.popoverPresentationController.permittedArrowDirections = arrowDirections;
    self.contentController.popoverPresentationController.delegate = self;

    if (self.popoverContentSize.width != 0 && self.popoverContentSize.height != 0) {
        self.contentController.preferredContentSize = self.popoverContentSize;
    }

    UIViewController *presentingViewController = [self parentControllerForView:view];
    if (!presentingViewController) {
        presentingViewController = [self topMostController];
    }

    [presentingViewController presentViewController:self.contentController animated:animated completion:nil];
}

/**
 Find the parent view controller of a view.
 @See https://stackoverflow.com/a/24590678/816799
 */
- (nullable UIViewController *)parentControllerForView:(UIView *)view {
    UIResponder *parentResponder = view;
    while (parentResponder != nil) {
        parentResponder = [parentResponder nextResponder];
        if ([parentResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)parentResponder;
        }
    }

    return nil;
}

- (nullable UIViewController *)topMostController {
    id<UIApplicationDelegate> delegate = [UIApplication sharedApplication].delegate;

    UIWindow *mainWindow;
    if ([delegate respondsToSelector:@selector(window)]) {
        mainWindow = delegate.window;
    }

    return [self findTopMostControllerWithRoot:mainWindow.rootViewController];
}

- (nullable UIViewController *)findTopMostControllerWithRoot:(nullable UIViewController *)root {
    if (root == nil) {
        return nil;
    }

    if ([root isKindOfClass:[UINavigationController class]]) {
        UINavigationController *controller = (UINavigationController *)root;
        return [self findTopMostControllerWithRoot:controller.visibleViewController];
    }

    if ([root isKindOfClass:[UITabBarController class]]) {
        UITabBarController *controller = (UITabBarController *)root;
        return [self findTopMostControllerWithRoot:controller.selectedViewController];
    }

    if (root.presentedViewController != nil) {
        return [self findTopMostControllerWithRoot:root.presentedViewController];
    }

    return root;

}

- (void)dismissPopoverAnimated:(BOOL)animated {
    [self.contentController dismissViewControllerAnimated:animated completion:nil];
}
#pragma mark - UIPopoverPresentationControllerDelegate

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {

    if ([self.delegate respondsToSelector:@selector(popoverControllerDidDismissPopover:)]) {
        [self.delegate popoverControllerDidDismissPopover:self];
    }
}

@end
