//
//  CCLogger.m
//  CCForce
//
//  Created by Henry Heguang Miao on 16/10/17.
//  Copyright © 2017 Coroma Consulting. All rights reserved.
//

#import "CCLogger.h"
#import "CCLoggerUtils.h"

@interface CCLogger ()


@end

@implementation CCLogger

+ (CCLogger *)sharedInstance {
    static CCLogger *shared;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[CCLogger alloc]init];
    });
    return shared;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        // Set the default log level
        [DDLog addLogger:[DDTTYLogger sharedInstance] withLevel:DDLogLevelError]; // TTY = Xcode console
//        [DDLog addLogger:[DDASLLogger sharedInstance] withLevel:DDLogLevelError]; // ASL = Apple System Logs
    }
    return self;
}

- (void)setLogLevel:(DDLogLevel)logLevel {
    _logLevel = logLevel;
    [DDLog removeAllLoggers];
    [DDLog addLogger:[DDTTYLogger sharedInstance] withLevel:logLevel]; // TTY = Xcode console
//    [DDLog addLogger:[DDASLLogger sharedInstance] withLevel:logLevel]; // ASL = Apple System Logs
}

+ (DDLogLevel)logLevel {
    return [CCLogger sharedInstance].logLevel;
}

+ (void)setLogLevel:(DDLogLevel)logLevel {
    [CCLogger sharedInstance].logLevel = logLevel;
}

+ (void)cc_log:(DDLogLevel)level msg:(NSString *)message {
    [self cc_log:level format:@"%@", message];
}

+ (void)cc_log:(DDLogLevel)level format:(NSString *)format, ... {
    CCLogger *logger = [CCLogger sharedInstance];
    va_list args;
    va_start(args, format);
    
    [logger cc_log:level != DDLogLevelError
          level:level
           flag:[CCLoggerUtils ddLogFlagForLogLevel:level]
        context:0
           file:nil
       function:nil
           line:0
            tag:nil
         format:format
           args:args];
    
    va_end(args);
}

- (void)cc_log:(BOOL)asynchronous
      level:(DDLogLevel)level
       flag:(DDLogFlag)flag
    context:(NSInteger)context
       file:(const char *)file
   function:(const char *)function
       line:(NSUInteger)line
        tag:(id)tag
     format:(NSString *)format
       args:(va_list)args {

    DDLog *logger = [DDLog sharedInstance];
    [logger log:level != DDLogLevelError
          level:level
           flag:flag
        context:context
           file:file
       function:function
           line:line
            tag:tag
         format:format
           args:args];
}

@end
