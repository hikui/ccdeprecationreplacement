//
//  CCLoggerUtils.h
//  CCDeprecationReplacement
//
//  Created by Henry Miao on 20/12/17.
//

#import <Foundation/Foundation.h>
#import <CocoaLumberjack/CocoaLumberjack.h>

@interface CCLoggerUtils : NSObject

+ (DDLogFlag)ddLogFlagForLogLevel:(DDLogLevel)level;

@end
