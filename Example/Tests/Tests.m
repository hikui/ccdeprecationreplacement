//
//  CCDeprecationReplacementTests.m
//  CCDeprecationReplacementTests
//
//  Created by Henry Heguang Miao on 11/21/2017.
//  Copyright (c) 2017 Henry Heguang Miao. All rights reserved.
//

@import XCTest;

@interface Tests : XCTestCase

@end

@implementation Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end

