//
//  main.m
//  CCDeprecationReplacement
//
//  Created by Henry Heguang Miao on 11/21/2017.
//  Copyright (c) 2017 Henry Heguang Miao. All rights reserved.
//

@import UIKit;
#import "CCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CCAppDelegate class]));
    }
}
