#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "CCAlertView.h"
#import "CCLogger.h"
#import "CCPopoverController.h"
#import "NSObject+CCLogging.h"

FOUNDATION_EXPORT double CCDeprecationReplacementVersionNumber;
FOUNDATION_EXPORT const unsigned char CCDeprecationReplacementVersionString[];

